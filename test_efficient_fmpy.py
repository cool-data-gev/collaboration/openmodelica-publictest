# Khurshed Ali, DTU Compute
""" This example demonstrates how to save CPU time by reusing the extracted FMU,
 loaded model description, and FMU instance when simulating the same FMU multiple times """

from fmpy import *
from fmpy.util import download_file
import shutil
from datetime import datetime
import pdb
import logging



def run_efficient_loop(y1_res, filename, iterations):
    logging.basicConfig(filename='.\logs\out_testOM_2MTR.log', filemode='w', format='%(asctime)s - %(message)s', level=logging.INFO)

    unzipdir = extract(filename)
    model_description = read_model_description(unzipdir)
    fmu_instance = instantiate_fmu(unzipdir, model_description, fmi_type='CoSimulation')

    init_time = datetime.now()
    print('Start Time: ', init_time)

    for i in range(iterations):
        logging.info(f'Iteration Number:  {i}')
        fmu_instance.reset()
        start_values = {'u1': 110 - 90*i, 'p1':y1_res}
        result = simulate_fmu(unzipdir,
                              start_values=start_values,
                              start_time=0,
                              stop_time=900,
                              model_description=model_description,
                              fmu_instance=fmu_instance)
        y1 = result['y1']
        y1_res = y1[-1]

    logging.info(f'Return Value: {y1_res}')   

    fmu_instance.freeInstance()
    shutil.rmtree(unzipdir, ignore_errors=True)

    fin_time = datetime.now()
    print('Start Time: ', init_time)
    print("Total Execution time: ", (fin_time-init_time))


if __name__ == '__main__':
    filename = 'FMU_loop_test.fmu'
    iterations = 2000000
    y1_res = 30;
    run_efficient_loop(y1_res, filename, iterations)
